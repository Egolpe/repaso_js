/**
 * ###################################
 * ###### E J E R C I C I O   1 ######
 * ###################################
 * 
 * Sara y Laura juegan al baloncesto en diferentes equipos. En los
 * últimos 3 partidos, el equipo de Sara anotó 89, 120 y 103 puntos,
 * mientras que el equipo de Laura anotó 116, 94, y 123 puntos.
 *
 * `1.` Calcula la media de puntos para cada equipo.
 *
 * `2.` Muestra un mensaje que indique cuál de los dos equipos
 *      tiene mejor puntuación media. Incluye en este mismo mensaje
 *      la media de los dos equipos.
 *
 * `3.` María también juega en un equipo de baloncesto. Su equipo
 *      anotó 97, 134 y 105 puntos respectivamente en los últimos
 *      3 partidos. Repite los pasos 1 y 2 incorporando al equipo
 *      de María.
 *
 */
 const teamSara = [89, 120, 103];
 const teamLaura = [116, 94, 123];

function valuePromedio(value) {
  const suma = value.reduce((previous, current) => (current += previous));
  const promedio = suma / value.length;
  return promedio;
}

const promedioTeamSara = valuePromedio(teamSara);
const promedioTeamLaura = valuePromedio(teamLaura);
console.log(promedioTeamSara);
console.log(promedioTeamLaura);

function maxPromedio(value1, value2, value3) {
  const win = [value1, value2, value3];
  const maxValue = Math.max(...win);
  if (maxValue === promedioTeamLaura) {
    console.log(
      `Las puntuaciones medias son ${value1} y ${value2}, y el mayor marcador lo obtuvo Laura`
    );
  } else if (maxValue === promedioTeamSara) {
    console.log(
      `Las puntuaciones medias son ${value1} y ${value2}, y el mayor marcador lo obtuvo Sara`
    );
  } else {
    console.log(
      `Las puntuaciones medias son ${value1}, ${value2} y ${value3}y el mayor marcador lo obtuvo Maria`
    );
  }
}

const teamMaria = [97, 134, 105];
const promedioTeamMaria = valuePromedio(teamMaria);
console.log(promedioTeamMaria);

maxPromedio(promedioTeamSara, promedioTeamLaura, promedioTeamMaria);

/**
 * ###################################
 * ###### E J E R C I C I O   2 ######
 * ###################################
 * 
 * Jorge y su familia han ido a comer a tres restaurantes distintos.
 * La factura fue de 124€, 58€ y 268€ respectivamente.
 * 
 * Para calcular la propina que va a dejar al camarero, Jorge ha 
 * decidido crear un sistema de calculo (una función). Quiere
 * dejar un 20% de propina si la factura es menor que 50€, un 15%
 * si la factura está entre 50€ y 200€, y un 10% si la factura es
 * mayor que 200€.
 * 
 * Al final, Jorge tendrá dos arrays:
 * 
 * - `Array 1` Contiene las propinas que ha dejado en cada uno de 
 *    los tres restaurantes.
 * 
 * - `Array 2` Contiene el total de lo que ha pagado en cada uno de
 *    los restaurantes (sumando la propina).
 * 
 * `NOTA` Para calcular el 20% de un valor, simplemente multiplica
 *  por `0.2`. Este resultado se obtiene de dividir `20/100`. Si
 *  quisieramos averiguar el 25% de un valor lo multiplicaríamos
 *  por 0.25.
 * 
 * `25 / 100 = 0.25`.
 * 
 */
 const factura = [124, 58, 268];

function calcularPropina(factura) {
  let propina = [];
  for (let index = 0; index < factura.length; index++) {
    if (factura[index] < 50) {
      const p1 = Math.ceil(factura[index] * 0.2);
      propina.push(p1);
    } else if (factura[index] >= 50 && factura[index] < 200) {
      const p2 = Math.ceil(factura[index] * 0.15);
      propina.push(p2);
    } else if (factura[index] > 200) {
      const p3 = Math.ceil(factura[index] * 0.1);
      propina.push(p3);
    }
  }
  return propina;
}

function calcularTotalPagado(factura, propinas) {
  let total = factura.map((current, index) => {
    return current + propinas[index];
  });
  return total;
  // const sum = total.reduce((previous, current) => (current += previous));
  // return sum;
}

console.log(factura);
const propinas = calcularPropina(factura);
console.log(propinas);
const totalPagado = calcularTotalPagado(factura, propinas);
console.log(totalPagado);

 /**
 * ###################################
 * ###### E J E R C I C I O   3 ######
 * ###################################
 * 
 * Dado el siguiente array de números:
 * 
 * `nums = [100, 3, 4, 2, 10, 4, 1, 10]`
 * 
 * `1.` Recorre todo el array y muestra por consola cada uno de sus
 *      elementos con la ayuda de un `for`, con la ayuda de un `map`
 *      y con la ayuda de un `for...of`.
 * 
 * `2.` Ordena el array de menor a mayor sin emplear `sort()`.
 * 
 * `3.` Ordena el array de mayor a menor empleando `sort()`.
 * 
 */
 const nums = [100, 3, 4, 2, 10, 4, 1, 10];
 
 function recorrerArray(nums){
     for (let index = 0; index < nums.length; index++){
         console.log(`El elemento es: ${nums[index]}`)
     }
     for(const num of nums){
       console.log(`El elemento es: ${num}`);
     }
     let numbers = nums.map((current, index) => {
       console.log(`El elemento es: ${nums[index]}`);
     });
 }
recorrerArray(nums);

function ordenarMenosMas(nums) {
  let newOrder = [...nums];
  for (let i = 0; i < nums.length; i++) {
    for (let j = 0; j < nums.length; j++) {
      if (newOrder[i] < newOrder[j]) {
        let save = newOrder[j];
        newOrder[j] = newOrder[i];
        newOrder[i] = save;
      }
    }
  }
  console.log(newOrder);
}
ordenarMenosMas(nums);

const order = nums.sort(ordered);
function ordered(a, b) {
  return b - a;
}
console.log(order);

  /**
 * ###################################
 * ###### E J E R C I C I O   4 ######
 * ###################################
 * 
 * Crea una `arrow function` que reciba dos números por medio del 
 * `prompt`, reste ambos números, y nos devuelva el resultado. 
 * En caso de que el resultado sea negativo debe cambiarse a 
 * positivo. Este resultado se mostrará por medio de un `alert`.
 * 
 */
 
 const deduct = () => {
  let num1 = +prompt("ingresa un numero");
  let num2 = +prompt("ingresa otro número");
  num1 -= num2;
  if (num1 < 0) {
    num1 = num1 * -1;
    alert(num1);
  } else {
    alert(`El resultado es: ${num1}`);
  }
};

deduct();


